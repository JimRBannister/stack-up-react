# Stack Up 

- [Getting started](#getting-started)
- [Develop a React app with ChartJS in no time Activity](#develop-a-react-app-with-chartjs-in-no-time)
- [References](#handy-references)

## Getting started

Please start by reading the [Stack Up Courseware](https://jimrbannister.gitlab.io/stack-up/#top).



Fork a new project from https://gitlab.com/JimRBannister/stack-up-react if you have already done so.

Unlink the fork see your new project settings -> General -> Advanced in GitLab.

Create a stackup projects directory if one doesn't exist. All projects will be held under this sub-directory including stack-up-react.

```bash
mkdir stackup
```

Change directory into your project directory.
```bash
cd stackup
```


Clone your new repository using git if the repository is not already held locally.

```bash
git clone <url>
```

Change directory into the new directory created by the clone.

```bash
cd <repo name>
```


Checkout the development branch
```bash
git checkout dev
```

Adapt the tag names below to reflect your requirements if you wish. Please also change the docker-compose yaml in your copy of the stack-up-administration repository to reflect these changes
The prefix, stackup, represents to the repository name, and you may wish to change this to reflect your repository name and naming conventions.
For example, you may wish to store your images in repository such Docker Hub or AWS Elastic Container Registry to reflect the repository name.

The environment will be set in the Docker Compose file found in the Stack Up Administration Project.

## Develop a React app with ChartJS in no time

The two main source files, index.js and app.js, are contained in the src/ directory.

The application uses ChartJS to display a graph of recent temperatures. These temperatures are retrieved using GraphQL every 10 seconds via a polling mechanism.

Build the docker image for this react application
```bash
docker build -t stackup/hartree-react .
```

Refer to the stack-up-administration project to continue this activity; it provides instructions on how to deploy.


## Handy References!

- [reactjs](https://reactjs.org/)
- [chart.js](https://www.chartjs.org/)
- [yarn](https://yarnpkg.com/)
- [Docker](https://www.docker.com/)
- [Docker compose](https://docs.docker.com/compose/)
