import React from 'react';
import {Line} from 'react-chartjs-2';
import moment from 'moment';
import {gql, useQuery} from "@apollo/client";


const GET_RECENT_TEMPERATURES = gql`
query RecentTempQuery {
  recentTemperatures {
    fivesecinterval
    location
    maxtemp
  }
}
`;

const BORDER_COLOURS = ["blue", "brown", "red"];

const chartData = {
    labels: [],
    datasets: []
};

export function RecentTemperatures() {

    const {loading, error, data} = useQuery(GET_RECENT_TEMPERATURES, {
        pollInterval: process.env.REACT_APP_GRAPHQL_POLL_INTERVAL,
        fetchPolicy: "no-cache"
    });

    if (loading) return null;
    if (error) return `Error! ${error}`;
    console.log(JSON.stringify(data.recentTemperatures))
    data.recentTemperatures.forEach((item) => {
        const humanReadableTime = item.fivesecinterval ? moment(item.fivesecinterval).format('LTS') : "";

        chartData.labels.push(humanReadableTime);

        let dataSetIndex = chartData.datasets.map(e => e.location).indexOf(item.location);

            if (dataSetIndex >= 0)
            {
                chartData.datasets[dataSetIndex].data.push(item.maxtemp);

            }
            else {
                if (chartData.datasets.length < BORDER_COLOURS.length)
                chartData.datasets.push({
                    label: item.location + " max. temp. every 5 seconds",
                    data:[],
                    pointBackgroundColor:[],
                    borderColor:BORDER_COLOURS[chartData.datasets.length],
                    fill:true,
                    location: item.location})
            }
    })

    return (
        <div
            style={{display: 'flex', alignItems: 'center', justifyContent: 'center', margin: '20px'}}
        >
            <Line
                data={chartData}
                options={{
                    animation: {duration: 0},
                    scales: {yAxes: [{ticks: {min: 5, max: 20}}]}
                }}
            />
        </div>
    )
}







