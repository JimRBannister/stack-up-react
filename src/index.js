import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {RecentTemperatures} from './App';

import {
  ApolloClient,
  ApolloProvider,
  InMemoryCache
} from '@apollo/client';

const cache = new InMemoryCache();

const client = new ApolloClient({
  cache,
  uri: `http://${process.env.REACT_APP_GRAPHQL_HOST}:${process.env.REACT_APP_GRAPHQL_PORT}/graphql`

});

ReactDOM.render(
    <ApolloProvider client={client}>
        <RecentTemperatures />
        </ApolloProvider>,
document.getElementById('root'),
);

